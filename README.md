This repo is used for INFO 5100 and INFO 5101 by group Dumpling(Shangrui Xie, Ziyuan Ren, Qiuchi Chen)

Lab5

Tasks:
Implement the abstract method in Customer class.
Add validation to the "AdminCreateScreen". 
Use Regex pattern matching to verify Username and Password.
Complete the customer login screen.
populate the Customer Table !! and ***** in the login screen should be replaced by text "Supplier" or "Customer" based on who is trying to log in 

This project was created by Netbeans. If you want to use it, please clone the repo and import it into NetBeans, and the JDK version is 1.7
