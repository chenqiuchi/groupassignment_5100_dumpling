/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Abstract.User;
import Business.Users.Supplier;
import java.util.ArrayList;
import java.util.List;
import sun.security.util.Password;

/**
 *
 * @author harshalneelkamal
 */
public class SupplierDirectory {
    
    private List<Supplier> supplierList;
    
    public SupplierDirectory(){
        supplierList = new ArrayList<>();
    }

    public List<Supplier> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(List<Supplier> supplierList) {
        this.supplierList = supplierList;
    }
    
    public Supplier addSupplier(String password, String userName){
        Supplier s = new Supplier(password, userName);
        supplierList.add(s);
        return s;
    }
    
    
    public void removeSupplier(Supplier s){
        supplierList.remove(s);
    }
    
    public Supplier searchSupplier(String supplierNmae){
        for(Supplier s: supplierList){
            if(s.getUserName().equals(supplierNmae) )
                return s;
        }
        return null;
    }
    
    
    
}
