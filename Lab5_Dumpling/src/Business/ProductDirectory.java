/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class ProductDirectory {
    
    private List<Product> productList;// = new ArrayList<Product>();
    
    public ProductDirectory(){
        productList = new ArrayList<>();
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
    
    public void delete(Product p){
      productList.remove(p);
  }
    
//    public Product addProduct(String name, double price, int ID, int availbility){
//        Product p = new Product(name, price, ID, availbility);
//        productList.add(p);
//        return p;
//    }
    
    public Product addProduct(){
        Product p = new Product();
        productList.add(p);
        return p;
    }
    
    public void removeProduct(Product p){
        productList.remove(p);
    }
    
    public Product searchProduct(String productName){
        for(Product p: productList){
            if(p.getName().equals(productName) )
                return p;
        }
        return null;
    }
    
    
}
