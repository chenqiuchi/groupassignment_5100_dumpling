/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author harshalneelkamal
 */
public class Product implements Comparable<Product>{
    private String name;
    private int ID;
    private int availibility;
    private double price;
//
//    public Product(String name, double price, int ID, int availbility) {
//        this.name = name;
//        this.price = price;
//        this.ID = ID;
//        this.availibility = availibility; 
//    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getAvailibility() {
        return availibility;
    }

    public void setAvailibility(int availibility) {
        this.availibility = availibility;
    }

    
    @Override
    public int compareTo(Product o) {
        return this.name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    

    
}
