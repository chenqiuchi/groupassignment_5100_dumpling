/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author rachealchen
 */
public class Business {
    
    private SupplierDirectory supplierDirectory;
    private MastOrderCatalog masterOrderCatalog;
    
    
    public Business()
    
    {
        supplierDirectory = new SupplierDirectory();
        masterOrderCatalog = new MastOrderCatalog();
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public MastOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MastOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }
    
    
    
    
    
    
}
